USE [Criadero]
GO
INSERT [dbo].[Direcciones] ([IdDireccion], [Calle], [Nro], [IdBarrio]) VALUES (1, N'Independencia', 234, 37)
INSERT [dbo].[Empleados] ([DNI], [TipoDNI], [Nombre], [Apellido], [IdDireccion], [FechaNacimiento], [Telefono]) VALUES (40233134, 1, N'Juanito', N'Martinez', 1, NULL, NULL)
INSERT [dbo].[Empleados] ([DNI], [TipoDNI], [Nombre], [Apellido], [IdDireccion], [FechaNacimiento], [Telefono]) VALUES (40566054, 1, N'Ortencia', N'Garcia', NULL, NULL, NULL)
INSERT [dbo].[Empleados] ([DNI], [TipoDNI], [Nombre], [Apellido], [IdDireccion], [FechaNacimiento], [Telefono]) VALUES (40681779, 1, N'Sergio', N'Ramirez', NULL, NULL, NULL)
INSERT [dbo].[Usuarios] ([NombreUsuario], [DniEmpleado], [TipoDNI], [Contraseña], [IdPermiso]) VALUES (N'Sergio_ramirez', 40681779, 1, N'1234', NULL)
